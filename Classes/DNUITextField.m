//
//  DNUITextField.m
//  MZ Proto
//
//  Created by Darren Ehlers on 3/5/13.
//  Copyright (c) 2013 DoubleNode.com. All rights reserved.
//

#import "DNUITextField.h"

@implementation DNUITextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
