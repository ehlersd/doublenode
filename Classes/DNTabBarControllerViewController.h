//
//  DNTabBarControllerViewController.h
//  DoubleNode.com
//
//  Created by Darren Ehlers on 10/16/12.
//  Copyright (c) 2012 DoubleNode.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DNTabBarControllerViewController : UITabBarController <UITabBarDelegate>

@end
