//
//  DNUITextField.h
//  MZ Proto
//
//  Created by Darren Ehlers on 3/5/13.
//  Copyright (c) 2013 DoubleNode.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DNUITextField : UITextField

@property (nonatomic, readwrite, assign) IBOutlet UIView*   nextField;

@end
